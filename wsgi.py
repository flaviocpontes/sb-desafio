# -*- coding: utf-8 -*-
import logging

from customer import create_app
from config import ProductionConfig

logging.basicConfig(filename='customer_crud.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s - %(message)s', datefmt='%d/%m/%Y %H:%M:%S')

app = create_app(ProductionConfig)
