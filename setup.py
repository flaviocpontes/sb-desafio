# -*- coding: utf-8 -*-
"""
Um CRUD com interface Web incompleta
"""


from setuptools import setup, find_packages
from customer import __version__, __author__, __author_email__
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='customer_crud',

    version=__version__,

    description='Um CRUD básico feito de uma API RESTful e um fronted AngularJS',
    long_description=long_description,

    # The project's main homepage.
    url='https://flaviocpontes@bitbucket.org/flaviocpontes/sb-desafio.git',

    # Author details
    author=__author__,
    author_email=__author_email__,

    # Choose your license
    license='Public Domain',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Recruiters',
        'License :: Public Domain',
        'Programming Language :: Python :: 3.5',
    ],

    # What does your project relate to?
    keywords='crud challenge',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=['customer'],

    install_requires=['flask', 'Flask-Testing', 'gunicorn', 'tinydb'],
)