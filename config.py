# -*- coding: utf-8 -*-
import os
basedir = os.path.abspath(os.path.dirname(__file__))


class ProductionConfig:
    DEBUG = False
    TESTING = False
    TEMP_DIR = '/tmp'
    TINYDB_DATABASE_URI = os.path.join(basedir, 'customer.db')
    LOGFILE = os.path.join(basedir, 'customer.log')


class DevelConfig:
    TESTING = False
    DEBUG = True
    USE_DEBUGGER = True;
    USE_RELOADER = False
    SECRET_KEY = 'secret'
    TEMP_DIR = os.path.join(basedir, 'temp')
    TINYDB_DATABASE_URI = os.path.join(TEMP_DIR, 'customer_devel.db')
    LOGFILE = os.path.join(TEMP_DIR, 'customer_devel.log')


class TestingConfig:
    TESTING = True
    DEBUG = True
    USE_DEBUGGER = False
    USE_RELOADER = False
    SECRET_KEY = 'secret'
    LIVESERVER_PORT = 8943
    TEMP_DIR = os.path.join(basedir, 'temp')
    TINYDB_DATABASE_URI = os.path.join(TEMP_DIR, 'customer_testing.db')
    LOGFILE = os.path.join(TEMP_DIR, 'customer_test.log')
