import json
import unittest

from config import TestingConfig
from customer import create_app
from tests.helper import get_customer_list


class TestUpdating(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestingConfig)
        self.app.db.purge()
        self.client = self.app.test_client()
        customer_list = get_customer_list()
        for customer in customer_list:
            self.app.db.insert(customer)

    def test_update_name(self):
        response = self.client.put('/api/clientes/1', data=json.dumps({'name': 'New Man'}))

        self.assertEqual(400, response.status_code)
        self.assertDictEqual({'status': 400, 'error': "can't set attribute",
                              'message': "can't set name attribute"},
                             json.loads(response.data.decode()))

    def test_update_id(self):
        response = self.client.put('/api/clientes/1', data=json.dumps({'id': 'New Man'}))

        self.assertEqual(400, response.status_code)
        self.assertDictEqual({'status': 400, 'error': "can't set attribute",
                              'message': "can't set id attribute"},
                             json.loads(response.data.decode()))

    def test_update_cpf(self):
        response = self.client.put('/api/clientes/1', data=json.dumps({'cpf': '000.000.000-00'}))

        self.assertEqual(400, response.status_code)
        self.assertDictEqual({'status': 400, 'error': "can't set attribute",
                              'message': "can't set cpf attribute"},
                             json.loads(response.data.decode()))

    def test_update_address(self):
        response = self.client.put('/api/clientes/1', data=json.dumps({'address': 'Rua Nova, s/n'}))

        self.assertEqual(200, response.status_code)
        self.assertEqual('Rua Nova, s/n', self.app.db.get(eid=1).get('address'))

    def test_update_phone(self):
        response = self.client.put('/api/clientes/5', data=json.dumps({'phone': '+1 857 1-800-77857'}))

        self.assertEqual(200, response.status_code)
        self.assertEqual('+1 857 1-800-77857', self.app.db.get(eid=5).get('phone'))

    def test_update_email(self):
        response = self.client.put('/api/clientes/5', data=json.dumps({'email': 'salvo@ceu.com.br'}))

        self.assertEqual(200, response.status_code)
        self.assertEqual('salvo@ceu.com.br', self.app.db.get(eid=5).get('email'))

    def test_update_with_invalid_email(self):
        response = self.client.put('/api/clientes/5', data=json.dumps({'email': 'naosalvoceu.com.br'}))

        self.assertEqual(400, response.status_code)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': "naosalvoceu.com.br is not a valid email",
                              'invalid-fields': 'email',
                              'invalid-value': 'naosalvoceu.com.br'},
                             json.loads(response.data.decode()))

    def test_update_one_domain(self):
        response = self.client.put('/api/clientes/7', data=json.dumps({'domains': ['salvo.ceu.com.br']}))

        self.assertEqual(200, response.status_code)
        self.assertEqual(['salvo.ceu.com.br'], self.app.db.get(eid=7).get('domains'))

    def test_update_multiple_domains(self):
        response = self.client.put('/api/clientes/7', data=json.dumps({'domains': ['salvo.ceu.com.br', 'jesus.com.br', 'www.aleluia.org']}))

        self.assertEqual(200, response.status_code)
        self.assertEqual(['salvo.ceu.com.br', 'jesus.com.br', 'www.aleluia.org'], self.app.db.get(eid=7).get('domains'))

    def test_update_domains_string(self):
        response = self.client.put('/api/clientes/7', data=json.dumps({'domains': 'salvo.ceu.com.br'}))

        self.assertEqual(200, response.status_code)
        self.assertEqual(['salvo.ceu.com.br'], self.app.db.get(eid=7).get('domains'))

    def test_update_domains_invalid(self):
        response = self.client.put('/api/clientes/7', data=json.dumps({'domains': '.salvo.ceu.com.br'}))

        self.assertEqual(400, response.status_code)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': ".salvo.ceu.com.br is not a valid domain",
                              'invalid-fields': 'domains',
                              'invalid-value': ['.salvo.ceu.com.br']
                              },
                             json.loads(response.data.decode()))
