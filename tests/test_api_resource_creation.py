import json
import unittest

from config import TestingConfig
from customer import create_app


class TestCreation(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

        self.app = create_app(TestingConfig)
        self.app.db.purge()
        self.client = self.app.test_client()

    def test_empty_request(self):
        response = self.client.post('/api/clientes/',
                                    content_type='application/json')

        self.assertEqual(response.status_code, 400)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': 'empty request data'},
                             json.loads(response.data.decode()), )

    def test_invalid_request_data(self):
        response = self.client.post('/api/clientes/',
                                    data='aslkhaskhaskasfkjhafskjfdslh',
                                    content_type='application/json')

        self.assertEqual(response.status_code, 400)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': 'invalid json: Expecting value: line 1 column 1 (char 0)'},
                             json.loads(response.data.decode()))

    def test_missing_name(self):
        customer_data = {
            'address': 'Rua dos bobos, nº 0',
            'cpf': '052783977-99',
            'email': 'jsilva@qualquerdominio.inf',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer_data),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 400)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': 'Field \"name\" is missing',
                              'missing-fields': 'name'},
                             json.loads(response.data.decode()))

    def test_missing_address(self):
        customer_data = {
            'name': 'José da Silva',
            'cpf': '052783977-99',
            'email': 'jsilva@qualquerdominio.inf',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer_data),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 400)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': 'Field \"address\" is missing',
                              'missing-fields': 'address'},
                             json.loads(response.data.decode()))

    def test_customer_with_invalid_cpf(self):
        customer_data = {
            'name': 'José da Silva',
            'cpf': '05278397757',
            'email': 'jsilva@qualquerdominio.inf',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer_data),
                                    content_type='application/json')

        self.assertEqual(400, response.status_code)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': 'CPF 05278397757 does\'t match the format xxx.xxx.xxx-xx',
                              'invalid-fields': 'cpf',
                              'invalid-value': '05278397757'},
                             json.loads(response.data.decode()))

    def test_invalid_email(self):
        customer_data = {
            'name': 'José da Silva',
            'cpf': '679.417.261-57',
            'email': 'jsilva@qualquerdominio.1',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer_data),
                                    content_type='application/json')

        self.assertEqual(400, response.status_code)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': "jsilva@qualquerdominio.1 is not a valid email",
                              'invalid-fields': 'email',
                              'invalid-value': 'jsilva@qualquerdominio.1'},
                             json.loads(response.data.decode()))

    def test_missing_name_and_CPF(self):
        customer_data = {
            'address': 'Rua dos bobos, nº 0',
            'email': 'jsilva@qualquerdominio.inf',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer_data),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 400)
        self.assertDictEqual({'status': 400, 'error': 'bad request',
                              'message': 'Fields \"cpf\", \"name\" are missing',
                              'missing-fields': ['cpf', 'name']},
                             json.loads(response.data.decode()))

    def test_valid_customer_with_single_domain(self):
        customer_data = {
            'name': 'José da Silva',
            'cpf': '679.417.261-57',
            'email': 'jsilva@qualquerdominio.inf',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer_data),
                                    content_type='application/json')

        self.assertEqual(201, response.status_code)
        self.assertEqual('http://localhost/api/clientes/1', response.headers['location'])
        self.assertEqual([customer_data], self.app.db.all())

    def test_valid_customer_with_multiple_domains(self):
        customer_data = {
            'name': 'José da Silva',
            'cpf': '679.417.261-57',
            'email': 'jsilva@qualquerdominio.inf',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br', 'www.meupub.bar.br', 'www.minhaloja.com.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer_data),
                                    content_type='application/json')

        self.assertEqual(201, response.status_code)
        self.assertEqual('http://localhost/api/clientes/1', response.headers['location'])
        self.assertEqual([customer_data], self.app.db.all())

    def test_two_valid_customers(self):
        customer1_data = {
            'name': 'José da Silva',
            'cpf': '052.783.977-99',
            'email': 'jsilva@qualquerdominio.inf',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }
        customer2_data = {
            'name': 'Fátima Bezerra Bragança',
            'cpf': '489.160.528-66',
            'email': 'jsilva@qualquerdominio.inf',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br', 'www.meupub.bar.br', 'www.minhaloja.com.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer1_data),
                                    content_type='application/json')

        self.assertEqual(201, response.status_code)
        self.assertEqual('http://localhost/api/clientes/1', response.headers['location'])

        response = self.client.post('/api/clientes/',
                                        data=json.dumps(customer2_data),
                                        content_type='application/json')

        self.assertEqual(201, response.status_code)
        self.assertEqual('http://localhost/api/clientes/2', response.headers['location'])
        self.assertEqual([customer1_data, customer2_data], self.app.db.all())

    def test_duplicate_cpf(self):
        customer1_data = {
            'name': 'José da Silva',
            'cpf': '052.783.977-99',
            'email': 'jsilva@qualquerdominio.inf',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br']
        }
        customer2_data = {
            'name': 'Fátima Bezerra Bragança',
            'cpf': '052.783.977-99',
            'email': 'jsilva@qualquerdominio.inf',
            'address': 'Rua dos bobos, nº 0',
            'phone': '+55 (21) 5555-5555',
            'domains': ['www.minhacasa.meu.br', 'www.meupub.bar.br', 'www.minhaloja.com.br']
        }

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer1_data),
                                    content_type='application/json')

        self.assertEqual(201, response.status_code)
        self.assertEqual('http://localhost/api/clientes/1', response.headers['location'])

        response = self.client.post('/api/clientes/',
                                    data=json.dumps(customer2_data),
                                    content_type='application/json')

        self.assertEqual(409, response.status_code)


