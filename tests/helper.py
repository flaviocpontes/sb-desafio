import json
import os
from copy import deepcopy

from config import basedir

TEST_CUSTOMER_FILE = os.path.join(basedir, 'tests/test_files/customer_list.json')


def get_customer_list():
    with open(TEST_CUSTOMER_FILE) as json_file:
        return json.load(json_file)


def add_id(record_list):
    new_list = []
    for num, el in enumerate(record_list):
        work_el = deepcopy(el)
        work_el['id'] = num+1
        new_list.append(work_el)
    return new_list