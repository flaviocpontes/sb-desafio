import json
import unittest

from config import TestingConfig
from customer import create_app
from tests.helper import get_customer_list


class TestDeleting(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestingConfig)
        self.app.db.purge()
        self.client = self.app.test_client()
        customer_list = get_customer_list()
        for customer in customer_list:
            self.app.db.insert(customer)

    def test_delete_valid_customer(self):
        num_records = len(self.app.db.all())
        response = self.client.delete('/api/clientes/1')
        self.assertEqual(200, response.status_code)

        response = self.client.get('/api/clientes/1')
        self.assertEqual(404, response.status_code)
        self.assertEqual(num_records-1, len(self.app.db.all()))

    def test_delete_inexistant_customer(self):
        response = self.client.delete('/api/clientes/105')
        self.assertEqual(404, response.status_code)