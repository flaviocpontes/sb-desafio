from time import sleep

from flask_testing import LiveServerTestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from customer import create_app
from config import TestingConfig
from tests.helper import get_customer_list


class CustomerListTests(LiveServerTestCase):
    def create_app(self):
        app = create_app(TestingConfig)
        app.db.purge()
        customer_list = get_customer_list()
        for customer in customer_list:
            app.db.insert(customer)
        return app

    def setUp(self):
        self.url_base = 'http://localhost:{}/'.format(TestingConfig.LIVESERVER_PORT)
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(10)
        self.browser.get(self.url_base)

    def test_basic_layout(self):
        browser = self.browser
        self.assertEqual(browser.title, 'Cadastro de Clientes')

        self.assertEqual(len([x for x in browser.find_element_by_id('customer-list').find_elements_by_tag_name('tr')]), 11)

    def test_delete_customer(self):
        browser = self.browser
        browser.find_element_by_id("apaga5").click()
        self.assertEqual(self.browser.title, 'Cadastro de Clientes')
        self.assertEqual(len([x for x in browser.find_element_by_id('customer-list').find_elements_by_tag_name('tr')]),
                         10)
        self.assertFalse(browser.find_elements_by_id('apaga5'))

    def test_update_customer(self):
        browser = self.browser
        browser.find_element_by_id("edita5").click()
        self.assertEqual(browser.current_url, self.url_base + '#/edita_cliente/5')

        browser.find_element_by_id('email-field').clear()
        browser.find_element_by_id('phone-field').clear()
        browser.find_element_by_id('address-field').clear()
        browser.find_element_by_id('domains-field').clear()
        browser.find_element_by_id('email-field').send_keys('teste2@teste.com')
        browser.find_element_by_id('phone-field').send_keys('+55 (21) 12345-6789')
        browser.find_element_by_id('address-field').send_keys('Av. dos testes, 001')
        browser.find_element_by_id('domains-field').send_keys('testeando.com.br, www.teste.com.br')
        browser.find_element_by_id('save-button').click()

        sleep(1)
        self.assertEqual(browser.current_url, self.url_base + '#/clientes/')
        self.assertEqual([x.text for x in browser.find_element_by_id('cust5').find_elements_by_tag_name('td')],
                         ['Arlete da Silva Sauro', 'teste2@teste.com', '["testeando.com.br","www.teste.com.br"]', ''])

    def test_insert_customer(self):
        browser = self.browser
        self.assertEqual(browser.title, 'Cadastro de Clientes')

        browser.find_element_by_id('btn-link-novo').click()
        self.assertEqual(browser.current_url, self.url_base + '#/novo_cliente/')

        browser.find_element_by_id('name-field').send_keys('Nome Teste')
        browser.find_element_by_id('cpf-field').send_keys('727.808.466-07')
        browser.find_element_by_id('email-field').send_keys('teste@teste.com')
        browser.find_element_by_id('phone-field').send_keys('+55 (21) 12345-6789')
        browser.find_element_by_id('address-field').send_keys('Av. dos testes, 001')
        browser.find_element_by_id('domains-field').send_keys('testeando.com.br, www.teste.com.br')
        browser.find_element_by_id('save-button').click()

        sleep(1)
        self.assertEqual(browser.current_url, self.url_base + '#/clientes/')
        self.assertEqual([x.text for x in browser.find_element_by_id('cust11').find_elements_by_tag_name('td')],
                         ['Nome Teste', 'teste@teste.com', '["testeando.com.br","www.teste.com.br"]', ''])


class AddCustomersTest(LiveServerTestCase):
    def create_app(self):
        app = create_app(TestingConfig)
        app.db.purge()
        return app

    def setUp(self):
        self.url_base = 'http://localhost:{}/'.format(TestingConfig.LIVESERVER_PORT)
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(10)
        self.browser.get(self.url_base)

    def test_add_one_customer(self):
        browser = self.browser
        self.assertEqual(browser.title, 'Cadastro de Clientes')

        browser.find_element_by_id('btn-link-novo').click()
        self.assertEqual(browser.current_url, self.url_base + '#/novo_cliente/')

        browser.find_element_by_id('name-field').send_keys('Nome Teste')
        browser.find_element_by_id('cpf-field').send_keys('727.808.466-07')
        browser.find_element_by_id('email-field').send_keys('teste@teste.com')
        browser.find_element_by_id('phone-field').send_keys('+55 (21) 12345-6789')
        browser.find_element_by_id('address-field').send_keys('Av. dos testes, 001')
        browser.find_element_by_id('domains-field').send_keys('testeando.com.br, www.teste.com.br')
        browser.find_element_by_id('save-button').click()

        sleep(1)
        self.assertEqual(browser.current_url, self.url_base + '#/clientes/')
        self.assertEqual([x.text for x in browser.find_element_by_id('cust1').find_elements_by_tag_name('td')],
                         ['Nome Teste', 'teste@teste.com', '["testeando.com.br","www.teste.com.br"]', ''])

        browser.find_element_by_id('btn-link-novo').click()
        self.assertEqual(browser.current_url, self.url_base + '#/novo_cliente/')

        browser.find_element_by_id('name-field').send_keys('Testando Teste')
        browser.find_element_by_id('cpf-field').send_keys('664.675.127-74')
        browser.find_element_by_id('email-field').send_keys('testando.teste@teste.com')
        browser.find_element_by_id('phone-field').send_keys('+55 (21) 12345-6789')
        browser.find_element_by_id('address-field').send_keys('Av. dos testes, 001')
        browser.find_element_by_id('domains-field').send_keys('testeando.com.br, www.teste.com.br')
        browser.find_element_by_id('save-button').click()

        sleep(1)
        self.assertEqual(browser.current_url, self.url_base + '#/clientes/')
        self.assertEqual(len([x for x in browser.find_element_by_id('customer-list').find_elements_by_tag_name('tr')]), 3)
        self.assertEqual([x.text for x in browser.find_element_by_id('cust2').find_elements_by_tag_name('td')],
                         ['Testando Teste', 'testando.teste@teste.com', '["testeando.com.br","www.teste.com.br"]', ''])

