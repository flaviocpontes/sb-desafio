import json
import unittest

from config import TestingConfig
from customer import create_app
from tests.helper import get_customer_list, add_id


class TestRetrieving(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

        self.app = create_app(TestingConfig)
        self.app.db.purge()
        self.client = self.app.test_client()
        customer_list = get_customer_list()
        for customer in customer_list:
            self.app.db.insert(customer)

    def test_list_clients(self):
        response = self.client.get('/api/clientes/')

        self.assertEqual(200, response.status_code)
        self.assertDictEqual({'customers': add_id(get_customer_list())}, json.loads(response.data.decode()))

    def test_list_ordered_by_name(self):
        response = self.client.get('/api/clientes/?order_by=name')

        sorted_customer_list = sorted(add_id(get_customer_list()), key=lambda x: x.get('name'))
        response_json = json.loads(response.data.decode())
        self.assertEqual(200, response.status_code)
        self.assertDictEqual({'customers': sorted_customer_list}, response_json)

    def test_retrieve_by_id(self):
        response = self.client.get('/api/clientes/1')

        customer_1 = {'id': 1, 'cpf': '052.783.977-99', 'address': 'Rua dos bobos, nº 0', 'name': 'José da Silva', 'domains': ['www.minhacasa.meu.br'], 'phone': '+55 (21) 5555-5555', 'email': 'jsilva@qualquerdominio.inf'}
        self.assertEqual(200, response.status_code)
        self.assertEqual(customer_1, json.loads(response.data.decode()))

    def test_retrieve_inexistent_id(self):
        response = self.client.get('/api/clientes/20')

        self.assertEqual(404, response.status_code)
        self.assertEqual({'status': 404, 'error': 'resource not found'}, json.loads(response.data.decode()))