import unittest

from customer import DataValidationError
from customer.model import get_cpf_if_valid, get_email_if_valid, get_domains_if_valid


class TestCPFValidator(unittest.TestCase):
    def test_invalid_cpf(self):
        self.assertRaises(DataValidationError, get_cpf_if_valid, '957.415.977-57')

    def test_invalid_formatting(self):
        self.assertRaises(DataValidationError, get_cpf_if_valid, '679-417-261-57')
        self.assertRaises(DataValidationError, get_cpf_if_valid, '679417261-57')
        self.assertRaises(DataValidationError, get_cpf_if_valid, '67941726157')

    def test_valid_cpf(self):
        self.assertEqual('679.417.261-57', get_cpf_if_valid('679.417.261-57'))


class TestEmailValidator(unittest.TestCase):
    def test_invalid_emails(self):
        self.assertRaises(DataValidationError, get_email_if_valid, 'emailinvalido')
        self.assertRaises(DataValidationError, get_email_if_valid, 'email@.invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'email@invalido.a')
        self.assertRaises(DataValidationError, get_email_if_valid, '.email@invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'email()*@invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'email@()%*invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'meu..email@invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'meu.email.@invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'meu.email.invalido.@invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'meu@email@invalido.com')
        self.assertRaises(DataValidationError, get_email_if_valid, 'meuemail@invalido.1om')

    def test_valid_emails(self):
        self.assertEqual('meuemail@valido.com', get_email_if_valid('meuemail@valido.com'))
        self.assertEqual('meu.email@valido.mesmo.com', get_email_if_valid('meu.email@valido.mesmo.com'))
        self.assertEqual('meu-email_100@valido.mesmo.com', get_email_if_valid('meu-email_100@valido.mesmo.com'))
        self.assertEqual('meu-email@1.br', get_email_if_valid('meu-email@1.br'))
        self.assertEqual('meu-email+legal@muito.bacana.br', get_email_if_valid('meu-email+legal@muito.bacana.br'))


class TestDomainValidator(unittest.TestCase):
    def test_invalid_domain(self):
        self.assertRaises(DataValidationError, get_domains_if_valid, '.teste.invalido.com')
        self.assertRaises(DataValidationError, get_domains_if_valid, 'teste.invalido.c')
        self.assertRaises(DataValidationError, get_domains_if_valid, '*invalido.com')
        self.assertRaises(DataValidationError, get_domains_if_valid, 'teste.invalido.c')
        self.assertRaises(DataValidationError, get_domains_if_valid, 'teste.invalido.1om')
        self.assertRaises(DataValidationError, get_domains_if_valid, ['valido.com', 'teste.invalido.1om'])
        self.assertRaises(DataValidationError, get_domains_if_valid, ['teste.invalido.1om', 'valido.com'])

    def test_valid_domains(self):
        self.assertEqual(['valido.com'], get_domains_if_valid('valido.com'))
        self.assertEqual(['valido.com'], get_domains_if_valid(['valido.com']))
        self.assertEqual(['valido1.com', 'www.valido.com.br', 'br.valido.com'],
                         get_domains_if_valid(['valido1.com', 'www.valido.com.br', 'br.valido.com']))

    def test_valid_domains_comma_separated(self):
        self.assertEqual(['valido.com', 'www.teste.com.br', 'www.republica.com.br'],
                         get_domains_if_valid('valido.com, www.teste.com.br, www.republica.com.br'))

    def test_invalid_domains_comma_separated(self):
        self.assertRaises(DataValidationError, get_domains_if_valid, '.teste.invalido.com, www.valido.com, teste.invalido.1om')
