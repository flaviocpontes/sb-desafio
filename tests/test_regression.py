import json
import unittest

from config import TestingConfig
from customer import create_app
from tests.helper import get_customer_list


class TestUpdatingDuplicateIdListingError(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestingConfig)
        self.app.db.purge()
        self.client = self.app.test_client()
        customer_list = get_customer_list()
        for customer in customer_list:
            self.app.db.insert(customer)

    def test_updating_creates_duplicate_id_on_listing(self):
        response = self.client.put('/api/clientes/1', data=json.dumps({'address': 'New Test Address',
                                                                       'phone': 'New Test Phone'
                                                                       }))

        self.assertEqual(200, response.status_code)
        response = self.client.get('/api/clientes/')
        print(response)