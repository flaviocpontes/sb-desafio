Desafio Site Blindado
=====================

Esta aplicação simples é a solução proposta à etapa Desafio Site Blindado do processo de seleção.

Descrição
---------

É um sistema muito simples que implementa tão somete a criação da entidade cliente e a listagem dos clientes cadastrados.
O sistema é composto por uma API RESTful escrita em _*Flask*_ como backend e uma interface web simples escrita em _*AngularJS*_ e implementa as 4 operações do CRUD.

A API promove validação extensiva dos campos, mas a interface Web não, de forma que é possível enviar dados inválidos para a API mas esta não os processará e responderá com status HTTP 400, descrevendo o primeiro erro que foi detectado.

Instalação e Execução
---------------------

O programa foi testado no ubuntu 16.04 e o processo de instalação descrito a seguir só foi testado nele.

`sudo apt update && sudo apt -y full-upgrade && sudo apt -y install python3-pip python3-venv python3-selenium git`

`wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb`

`sudo apt install ./google-chrome-stable_current_amd64.deb`

`wget https://chromedriver.storage.googleapis.com/2.25/chromedriver_linux64.zip`

`unzip chromedriver_linux_64.zip`

`sudo mv chromedriver /usr/local/bin`

`git clone https://bitbucket.org/flaviocpontes/sb-desafio`

`cd sb-desafio`

`python3 -m venv venv`

`source venv/bin/activate`

`pip install -r requirements`

`python -m unittest`

`gunicorn --workers 3 --bind 0.0.0.0:8000 wsgi:app`

API
---

A API consiste do recurso `customer`, e faz a validação dos campos CPF, email e domínios. Os outros campos são obrigatórios, mas livres.

O endpoint /api/clientes/ pode receber tanto GET, para listar os clientes quanro POST, para criár um novo cliente.
O endpoint /api/cliente/:id pode receber GET, para pegar o cliente pela ID, PUT, para modificar um cliente através da id e DELETE para deletar um cliente.

Em suma:
- /api/clientes/ - GET, POST
- /api/clientes/:id - GET, PUT, DELETE.


Interface Web
-------------

A interface web pode ser encontrada em /#/clientes/ e traz uma listagem dos clientes cadastrados, e a opção de incluir novos clientes.
Além disso a listagem provê a edição e remoção dos registros correntes através de botões do lado direito da tabela.


Defeitos Conhecidos
-------------------
* Não foi possível implementar as máscaras de edição nas telas de inclusão e edição dos campos
* Não faz o tratamento dos erros reportados pela API. Originalmente deveria fazer a própria validação, mas isso não foi possível nos campos CPF e domínios.
