import json
import re

from flask import url_for

from customer.exceptions import DataValidationError, MissingDataFieldError, InvalidDataFieldError


class Customer():
    def __init__(self, **kwargs):
        super().__init__()
        self.id = kwargs.get('id')
        self._name = kwargs['name']
        self._cpf = get_cpf_if_valid(kwargs['cpf'])
        self.address = kwargs['address']
        self.phone = kwargs['phone']
        self._email = get_email_if_valid(kwargs['email'])
        self._domains = get_domains_if_valid(kwargs['domains'])

    @property
    def name(self):
        return self._name

    @property
    def cpf(self):
        return self._cpf

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        self._email = get_email_if_valid(email)

    @property
    def domains(self):
        return list(self._domains)

    @domains.setter
    def domains(self, domains):
        self._domains = get_domains_if_valid(domains)

    @staticmethod
    def editable_fields():
        return ['address', 'phone', 'email', 'domains']

    def get_url(self):
        return url_for('get_customer', id=self.id, _external=True)

    def export_data(self):
        result = {
            'name': self.name,
            'cpf': self.cpf,
            'address': self.address,
            'phone': self.phone,
            'email': self.email,
            'domains': self.domains
        }
        if self.id:
            result['id'] = self.id
        return result

    @staticmethod
    def from_data(request_data):
        return Customer(**validate_customer_data(request_data))


def validate_customer_data(request_data):
    def validate_customer_record(customer_data):
        missing = {'name', 'address', 'cpf', 'email', 'phone', 'domains'}.difference(set(customer_data.keys()))
        if missing:
            raise MissingDataFieldError('Field \"{}\" is missing'.format(list(missing)[0]) if len(missing) == 1 else
                                        'Fields \"{}\" are missing'.format('\", \"'.join(sorted(missing))),
                                        list(missing)[0] if len(missing) == 1 else sorted(list(missing)))

    def get_customer_data(request_data):
        if not request_data:
            raise DataValidationError('empty request data')
        try:
            return json.loads(request_data.decode())
        except json.JSONDecodeError as e:
            raise DataValidationError('invalid json: {}'.format(e.args[0]))

    customer_data = get_customer_data(request_data)
    validate_customer_record(customer_data)
    return customer_data


def get_cpf_if_valid(cpf):
    def validate_format(cpf):
        if not re.match('(\d\d\d)\.(\d\d\d)\.(\d\d\d)-(\d\d)', cpf):
            raise InvalidDataFieldError('CPF {} does\'t match the format xxx.xxx.xxx-xx'.format(cpf),
                                        'cpf', cpf)

    def validate_digits(cpf):
        digits = ''.join(re.match('(\d\d\d)\.(\d\d\d)\.(\d\d\d)-(\d\d)', cpf).groups())
        for digit in range(10, 12):
            acc = 0
            for num, multiplier in enumerate(range(digit, 1, -1)):
                acc += (int(digits[num]) * multiplier)
            mod = ((acc * 10) % 11) if ((acc * 10) % 11) != 10 else 0
            if mod != int(digits[digit-1]):
                raise InvalidDataFieldError('The CPF {} is invalid.'.format(cpf),
                                            'cpf', cpf)

    validate_format(cpf)
    validate_digits(cpf)
    return cpf


def get_email_if_valid(email):
    if not re.match("^[_A-Za-z0-9-+]+(\.?[a-zA-Z0-9_+-])+@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}$", email):
        raise InvalidDataFieldError("{} is not a valid email".format(email), 'email', email)
    return email


def get_domains_if_valid(domains):
    def validate_domain(domain):
        if not re.match("^([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}$", domain):
            raise InvalidDataFieldError("{} is not a valid domain".format(domain),
                                        'domains', domains)

    if type(domains) == str:
        domains = domains.split(',')
        domains = [domain.strip() for domain in domains]

    if type(domains) == list:
        for domain in domains:
            validate_domain(domain)
        return domains

    raise InvalidDataFieldError("{} are not valid domains".format(domains), 'domains', domains)
