import json
import logging

from flask import Flask, jsonify, request, send_from_directory, send_file, abort
from tinydb import TinyDB, where

from .model import Customer
from .exceptions import *

__author__ = 'Flávio Cardoso Pontes'
__author_email__ = '<flaviopontes@gmail.com>'
__version_info__ = (1, 1, 1)
__version__ = '.'.join(map(str, __version_info__))
__package__ = 'customer'


def create_app(config_module):
    logging.basicConfig(filename=config_module.LOGFILE,
                        level=logging.DEBUG if config_module.DEBUG else logging.INFO,
                        format='%(asctime)s %(levelname)s - %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
    logging.info('APP - Creating App.')
    app = Flask(__name__, static_url_path='')
    app.config.from_object(config_module)
    app.db = TinyDB(app.config['TINYDB_DATABASE_URI'])

    @app.errorhandler(DataValidationError)
    def invalid_request(e):
        response = jsonify({'status': 400,
                            'error': 'bad request',
                            'message': e.message})
        response.status_code = 400
        return response

    @app.errorhandler(MissingDataFieldError)
    def invalid_request(e):
        response = jsonify({'status': 400,
                            'error': 'bad request',
                            'message': e.message,
                            'missing-fields': e.missing_field})
        response.status_code = 400
        return response

    @app.errorhandler(InvalidDataFieldError)
    def invalid_request(e):
        response = jsonify({'status': 400,
                            'error': 'bad request',
                            'message': e.message,
                            'invalid-fields': e.invalid_field,
                            'invalid-value': e.invalid_value})
        response.status_code = 400
        return response

    @app.errorhandler(DuplicateKeyError)
    def conflict_request(e):
        response = jsonify({'status': 409,
                            'error': 'conflict',
                            'message': e.args[0]})
        response.status_code = 409
        return response

    @app.errorhandler(AttributeError)
    def invalid_attribute(e):
        response = jsonify({'status': 400,
                            'error': 'can\'t set attribute',
                            'message': e.args[0]})
        response.status_code = 400
        return response

    @app.errorhandler(404)
    def internal_server_error(e):
        response = jsonify({'status': 404, 'error': 'resource not found'})
        response.status_code = 404
        return response

    @app.errorhandler(500)
    def internal_server_error(e):
        response = jsonify({'status': 500, 'error': 'internal server error',
                            'message': e.args[0]})
        response.status_code = 500
        return response

    @app.route('/api/clientes/', methods=['POST'])
    def new_customer():
        logging.debug('NEW CUSTOMER: {}'.format(request.data))
        customer = Customer.from_data(request.data)
        if app.db.search(where('cpf') == customer.cpf):
            raise DuplicateKeyError('CPF {} is already in the database'.format(customer.cpf))
        customer.id = app.db.insert(customer.export_data())
        return jsonify({}), 201, {'Location': customer.get_url()}

    @app.route('/api/clientes/', methods=['GET'])
    def list_customers():
        logging.debug('GET CUSTOMERS: {}'.format(request.data))
        if request.args.get('order_by') == 'name':
            return jsonify({'customers': sorted([Customer(**{**{'id': el.eid}, **el}).export_data() for el in app.db.all()],
                                                key=lambda x: x.get('name'))})
        return jsonify({'customers': [Customer(**{**{'id': el.eid}, **el}).export_data() for el in app.db.all()]})

    @app.route('/api/clientes/<int:id>', methods=['GET'])
    def get_customer(id):
        logging.debug('GET CUSTOMER {}: {}'.format(id, request.data))
        customer_data = app.db.get(eid=id)
        if customer_data:
            return jsonify(Customer(**{**{'id': customer_data.eid}, **customer_data}).export_data())
        abort(404)

    @app.route('/api/clientes/<int:id>', methods=['PUT'])
    def update_customer(id):
        logging.debug('PUT CUSTOMER {}: {}'.format(id, request.data))
        request_data = json.loads(request.data.decode())

        for key in request_data.keys():
            if key not in Customer.editable_fields():
                raise AttributeError("can't set {} attribute".format(key))

        customer_data = app.db.get(eid=id)
        if customer_data:
            customer = Customer(**{**{'id': customer_data.eid}, **customer_data})
            for key in request_data.keys():
                if key == 'address':
                    customer.address = request_data[key]
                if key == 'phone':
                    customer.phone = request_data[key]
                if key == 'email':
                    customer.email = request_data[key]
                if key == 'domains':
                    customer.domains = request_data[key]
            app.db.update(customer.export_data(), eids=[id])
            return jsonify({})

        abort(404)

    @app.route('/api/clientes/<int:id>', methods=['DELETE'])
    def delete_customer(id):
        logging.debug('DELETE CUSTOMER {}'.format(id))
        customer_data = app.db.get(eid=id)
        if customer_data:
            app.db.remove(eids=[id])
            return jsonify({})
        abort(404)


    @app.route('/', methods=['GET'])
    def index():
        logging.debug('INDEX REQUEST: {}'.format(request.data))
        return send_file('static/index.html')

    return app