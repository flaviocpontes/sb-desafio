class DataValidationError(ValueError):
    message = None

    def __init__(self, *args):
        super().__init__(*args)
        self.message = args[0]


class MissingDataFieldError(DataValidationError):
    missing_field = None

    def __init__(self, *args):
        super().__init__(*args)
        self.missing_field = args[1]


class InvalidDataFieldError(DataValidationError):
    invalid_field = None
    invalid_value = None

    def __init__(self, *args):
        super().__init__(*args)
        self.invalid_field = args[1]
        self.invalid_value = args[2]


class DuplicateKeyError(Exception):
    pass


class IllegalDataUpdate(Exception):
    pass
