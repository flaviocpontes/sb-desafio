(function () {

    function CustomerUpdateController($scope, $routeParams, customerProvider) {
        console.log("CustomerUpdateController configuration");
        console.log($routeParams);

        $scope.customer = {
            id: null,
            name: null,
            cpf: null,
            email: null,
            phone: null,
            address: null,
            domains: []
        };

        $scope.updateCustomer = function() {
            send_data = {
                email: this.customer.email,
                phone: this.customer.phone,
                address: this.customer.address,
                domains: this.customer.domains
            };
            customerProvider.updateCustomer(this.customer.id, send_data, function (err, customer) {
                console.log(err);
                if (err) {
                    console.log(err.message);
                } else {
                    window.location = '/#/clientes/'
                }
            })
        };

        function getCustomer (customer_id)  {
            $scope.customer = customerProvider.getCustomer(customer_id, function(err, customer) {
                if (err) {
                    console.log(err.message);
                } else {
                    console.log(customer);
                    $scope.customer = customer;
                }

            })
        }

        getCustomer($routeParams.customer_id);

    }

    customerCrud.controller("CustomerUpdateController", ['$scope', '$routeParams','customerProvider', CustomerUpdateController]);

})();