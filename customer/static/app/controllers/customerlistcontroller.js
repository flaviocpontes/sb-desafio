(function () {

    function CustomerListController ($scope, $window, customerProvider) {
        console.log("CustomerListController configuration");

        $scope.page_load_error = null;
        $scope.finished_loading = false;

        $scope.deleteCustomer = function(customer_id) {
            $scope.customer = customerProvider.deleteCustomer(customer_id, function(err, customer) {
                console.log("Deletando Cliente " + customer_id);
                if (err) {
                    console.log(err.message);
                } else {
                    console.log("Cliente " + customer_id + " deletado com sucesso");
                    $window.location.href = "/#/clientes/";
                }
            })
        };

        function get_customers() {
            $scope.customers = customerProvider.getAllCustomers(function(err, customers) {
                $scope.finished_loading = true;
                if (err) {
                    $scope.page_load_error = err.message;
                } else {
                    console.log(customers);
                    $scope.customers = customers;
                }
            });
        }

        get_customers();
    }

    customerCrud.controller("CustomerListController", ['$scope', '$window', 'customerProvider', CustomerListController]);

})();