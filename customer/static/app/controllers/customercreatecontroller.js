(function () {

    function CustomerCreateController($scope, customerProvider) {
        console.log("CustomerCreateController configuration");

        $scope.new_customer = {
            name: null,
            cpf: null,
            email: null,
            phone: null,
            address: null,
            domains: []
        };

        $scope.newCustomer = function () {
            customerProvider.newCustomer(this.new_customer, function(err, customer) {
                console.log(err);
                if (err) {
                    $scope.new_customer_error = "(" + err.error + ") " + err.message;
                } else {
                    window.location = '/#/clientes/';
                }
            });
        }

    }

    customerCrud.controller("CustomerCreateController", ['$scope', 'customerProvider', CustomerCreateController]);

})();