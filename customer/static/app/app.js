var customerCrud = angular.module("customerCrud", [ "ngRoute" ]);

customerCrud.config(function ($routeProvider) {
    $routeProvider
        .when("/clientes/", { controller: "CustomerListController", templateUrl: "/app/partials/customer_list.html" })
        .when("/novo_cliente/", { controller: "CustomerCreateController", templateUrl: "/app/partials/customer_new.html" })
        .when("/edita_cliente/:customer_id", { controller: "CustomerUpdateController", templateUrl: "/app/partials/customer_update.html" })
        .when("/apaga_cliente/:customer_id", { controller: "CustomerUpdateController", redirectTo: "/clientes/" })
        .when("/", { redirectTo: "/clientes/" })
        .otherwise({ redirectTo: "/404_page", templateUrl: "/app/partials/404.html" });
});