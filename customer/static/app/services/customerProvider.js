(function () {

    function customerProvider($http) {

        this._server_host = "";

        this.getAllCustomers = function (done) {
            $http.get(this._server_host + "/api/clientes/")
                .success(function (data, status, header, conf) {
                    console.log(data);
                    done(null, data);
                })
                .error(function (data, status, headers, conf) {
                    console.log(data);
                    done(data)
                });
        };

        this.getCustomer = function (id, done) {
            $http.get(this._server_host + "/api/clientes/" + id)
                .success(function (data, status, headers, conf) {
                    done(null, data);
                })
                .error(function (data, status, headers, conf) {
                    done(data);
                });
        };

        this.newCustomer = function (customer_data, done) {
            $http.post(this._server_host + "/api/clientes/", customer_data)
                .success(function (data, status, headers, conf) {
                    done(null, data);
                })
                .error(function (data, status, headers, conf) {
                    done(data);
                });
        };

        this.updateCustomer = function (id, customer_data, done) {
            console.log(customer_data);
            $http.put(this._server_host + "/api/clientes/" + id, customer_data)
                .success(function (data, status, headers, conf) {
                    done(null, data);
                })
                .error(function (data, status, headers, conf) {
                    done(data);
                });
        };

        this.deleteCustomer = function (customer_id, done) {
            console.log(customer_id);
            $http.delete(this._server_host + "/api/clientes/" + customer_id)
                .success(function (data, status, headers, conf) {
                    console.log(data);
                    done(null, data);
                })
                .error(function (data, status, headers, conf) {
                    done(data);
                });
        }

    }

    customerCrud.service("customerProvider", ["$http", customerProvider]);

})();
