import os
import logging

from customer import create_app
from config import DevelConfig

if __name__ == '__main__':
    if not os.path.exists(DevelConfig.TEMP_DIR):
        os.makedirs(DevelConfig.TEMP_DIR)

    logging.basicConfig(filename=DevelConfig.LOGFILE, level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s - %(message)s', datefmt='%d/%m/%Y %H:%M:%S')

    logging.info('CREATING APP OBJECT')
    app = create_app(config_module=DevelConfig)
    app.run(host='0.0.0.0')
